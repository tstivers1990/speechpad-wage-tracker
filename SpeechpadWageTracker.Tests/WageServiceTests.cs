﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpeechpadWageTracker.Tests
{
    [TestClass]
    public class WageServiceTests
    {
        private WageService sut;

        [TestInitialize]
        public void TestInitialize()
        {
            sut = new WageService();
        }

        [TestMethod]
        public void ToggleTracking_SetTrackingTrueIfStartTracking()
        {
            sut.ToggleTracking();
            Assert.IsTrue(sut.Tracking);
        }

        [TestMethod]
        public void ToggleTracking_SetTrackingFalseIfStopTracking()
        {
            sut.ToggleTracking();
            sut.ToggleTracking();
            Assert.IsFalse(sut.Tracking);
        }

        [TestMethod]
        public void ToggleTracking_SetStartTimeIfStartTracking()
        {
            sut.ToggleTracking();
            Assert.AreNotEqual(DateTime.MinValue, sut.StartTime);
        }

        [TestMethod]
        public void ToggleTracking_SetEndTimeIfStopTracking()
        {
            sut.ToggleTracking();
            sut.ToggleTracking();
            Assert.AreNotEqual(DateTime.MinValue, sut.EndTime);
        }

        [TestMethod]
        public void ToggleTracking_IncreaseTotalTimeIfStopTracking()
        {
            sut.ToggleTracking();
            Thread.Sleep(1);
            sut.ToggleTracking();
            Assert.AreNotEqual(DateTime.MinValue, sut.TotalTime);
        }

        [TestMethod]
        public void Reset_TrueIfNotTracking()
        {
            bool result = sut.Reset();
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Reset_FalseIfTracking()
        {
            sut.ToggleTracking();
            bool result = sut.Reset();
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Reset_ClearStartTime()
        {
            sut.ToggleTracking();
            Thread.Sleep(1);
            sut.ToggleTracking();
            sut.Reset();
            Assert.AreEqual(DateTime.MinValue, sut.StartTime);
        }

        [TestMethod]
        public void Reset_ClearEndTime()
        {
            sut.ToggleTracking();
            Thread.Sleep(1);
            sut.ToggleTracking();
            sut.Reset();
            Assert.AreEqual(DateTime.MinValue, sut.EndTime);
        }

        [TestMethod]
        public void Reset_ClearTotalTime()
        {
            sut.ToggleTracking();
            Thread.Sleep(1);
            sut.ToggleTracking();
            sut.Reset();
            Assert.AreEqual(TimeSpan.MinValue, sut.TotalTime);
        }
    }
}
