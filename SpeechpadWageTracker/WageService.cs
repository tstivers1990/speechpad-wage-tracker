﻿using System;

namespace SpeechpadWageTracker
{
    public class WageService
    {
        public decimal PayPerMinute { get; set; }
        public TimeSpan CompletedAudioTime { get; set; }

        public DateTime StartTime { get; private set; }
        public DateTime EndTime { get; private set; }
        public TimeSpan TotalTime { get; private set; }
        public bool Tracking { get; private set; }

        public decimal TotalPay
        {
            get
            {
                return PayPerMinute * (decimal)CompletedAudioTime.TotalMinutes;
            }
        }

        public decimal HourlyWage
        {
            get
            {
                if (TotalPay <= 0.00m || TotalTime.TotalHours <= 0.00)
                    return 0.00m;

                return TotalPay / (decimal)TotalTime.TotalHours;
            }
        }

        public void ToggleTracking()
        {
            if (!Tracking)
            {
                Tracking = true;
                StartTime = DateTime.Now;
            }
            else
            {
                EndTime = DateTime.Now;
                TimeSpan timeSpent = EndTime.Subtract(StartTime);
                TotalTime += timeSpent;
                Tracking = false;
            }
        }

        public bool Reset()
        {
            if (Tracking)
                return false;

            StartTime = DateTime.MinValue;
            EndTime = DateTime.MinValue;
            TotalTime = TimeSpan.MinValue;

            return true;
        }
    }
}
