﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SpeechpadWageTracker.Hotkey
{
    public class NativeMethods
    {
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool RegisterHotKey(
                                    IntPtr windowHandle,
                                    int hotkeyIdentifier,
                                    uint modifierCode,
                                    uint keyCode);
    }
}
