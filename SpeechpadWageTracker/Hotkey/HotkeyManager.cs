﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpeechpadWageTracker.Hotkey
{
    public class HotkeyManager
    {
        private HotkeyWindow _hotkeyWindow;
        private int _hotkeyId;

        public event EventHandler<HotkeyPressedEventArgs> HotkeyPressed = delegate { };

        public HotkeyManager(WageService wageService)
        {
            _hotkeyWindow = new HotkeyWindow(wageService, this);

        }

        public void RegisterHotkey(Modifiers modifiers, Keys keys)
        {
            bool success = NativeMethods.RegisterHotKey(_hotkeyWindow.Handle,
                                         _hotkeyId,
                                         (uint)modifiers,
                                         (uint)keys);

            if (!success)
            {
                throw new Win32Exception();
            }

            _hotkeyId++;
        }

        private sealed class HotkeyWindow : NativeWindow, IDisposable
        {
            private WageService _wageService;
            private readonly HotkeyManager _parent;

            public HotkeyWindow(WageService wageService, HotkeyManager parent)
            {
                _wageService = wageService;
                _parent = parent;
                CreateHandle(new CreateParams());
            }

            protected override void WndProc(ref Message m)
            {
                const int WM_HOTKEY = 0x0312;

                if (m.Msg == WM_HOTKEY)
                {
                    int lParam = (int)m.LParam;
                    var modifier = (Modifiers)(lParam & 0xFFFF);
                    var key = (Keys)(lParam >> 16);

                    _parent.HotkeyPressed(this, new HotkeyPressedEventArgs(modifier, key));
                }

                base.WndProc(ref m);
            }

            public void Dispose()
            {
                DestroyHandle();
            }
        }
    }
}
