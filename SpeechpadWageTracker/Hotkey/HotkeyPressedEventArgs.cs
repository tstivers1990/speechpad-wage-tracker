﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpeechpadWageTracker.Hotkey
{
    public class HotkeyPressedEventArgs
    {
        public Modifiers Modifiers { get; private set; }
        public Keys Keys { get; private set; }

        public HotkeyPressedEventArgs(Modifiers modifiers, Keys keys)
        {
            Modifiers = modifiers;
            Keys = keys;
        }
    }   
}
