﻿using SpeechpadWageTracker.Hotkey;
using System;
using System.Windows.Forms;

namespace SpeechpadWageTracker
{
    public partial class MainForm : Form
    {
        private WageService _wageService;
        private HotkeyManager _hotkeyManager;

        public MainForm()
        {
            _wageService = new WageService();
            InitializeComponent();
            initializeHotkeyManager();
        }

        private void initializeHotkeyManager()
        {
            _hotkeyManager = new HotkeyManager(_wageService);
            _hotkeyManager.HotkeyPressed += _hotkeyManager_HotkeyPressed;
            _hotkeyManager.RegisterHotkey(Modifiers.Control, Keys.OemPipe);
        }

        private void updateTotalPayLabel()
        {
            LabelTotalPay.Text = "Total Pay: " + _wageService.TotalPay.ToString("C");
        }

        private void updateHourlyWageLabel()
        {
            LabelHourlyWage.Text = "Hourly Wage: " + _wageService.HourlyWage.ToString("C");
        }

        private void _hotkeyManager_HotkeyPressed(object sender, HotkeyPressedEventArgs e)
        {
            _wageService.ToggleTracking();
            if (_wageService.Tracking)
            {
                ButtonReset.Enabled = false;
            }
            else
            {
                updateHourlyWageLabel();
                ButtonReset.Enabled = true;
            }
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            if (!_wageService.Reset())
                MessageBox.Show("Unable to reset. Please restart the program.");
        }

        private void DateTimePickerCompletedAudioTime_ValueChanged(object sender, EventArgs e)
        {
            _wageService.CompletedAudioTime = new TimeSpan(
                DateTimePickerCompletedAudioTime.Value.Hour,
                DateTimePickerCompletedAudioTime.Value.Minute,
                DateTimePickerCompletedAudioTime.Value.Second);

            updateTotalPayLabel();
            updateHourlyWageLabel();
        }

        private void NumericUpDownPayPerMinute_ValueChanged(object sender, EventArgs e)
        {
            _wageService.PayPerMinute = NumericUpDownPayPerMinute.Value;

            updateTotalPayLabel();
            updateHourlyWageLabel();
        }
    }
}
