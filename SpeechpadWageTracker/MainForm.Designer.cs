﻿namespace SpeechpadWageTracker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumericUpDownPayPerMinute = new System.Windows.Forms.NumericUpDown();
            this.DateTimePickerCompletedAudioTime = new System.Windows.Forms.DateTimePicker();
            this.LabelCompletedAudioTime = new System.Windows.Forms.Label();
            this.LabelPayPerMinute = new System.Windows.Forms.Label();
            this.ButtonReset = new System.Windows.Forms.Button();
            this.LabelTotalPay = new System.Windows.Forms.Label();
            this.LabelHourlyWage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPayPerMinute)).BeginInit();
            this.SuspendLayout();
            // 
            // NumericUpDownPayPerMinute
            // 
            this.NumericUpDownPayPerMinute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NumericUpDownPayPerMinute.DecimalPlaces = 2;
            this.NumericUpDownPayPerMinute.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NumericUpDownPayPerMinute.Location = new System.Drawing.Point(228, 38);
            this.NumericUpDownPayPerMinute.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            131072});
            this.NumericUpDownPayPerMinute.Name = "NumericUpDownPayPerMinute";
            this.NumericUpDownPayPerMinute.Size = new System.Drawing.Size(44, 20);
            this.NumericUpDownPayPerMinute.TabIndex = 0;
            this.NumericUpDownPayPerMinute.ValueChanged += new System.EventHandler(this.NumericUpDownPayPerMinute_ValueChanged);
            // 
            // DateTimePickerCompletedAudioTime
            // 
            this.DateTimePickerCompletedAudioTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DateTimePickerCompletedAudioTime.CustomFormat = "HH:mm:ss";
            this.DateTimePickerCompletedAudioTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePickerCompletedAudioTime.Location = new System.Drawing.Point(205, 12);
            this.DateTimePickerCompletedAudioTime.MaxDate = new System.DateTime(2000, 1, 1, 23, 59, 59, 999);
            this.DateTimePickerCompletedAudioTime.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateTimePickerCompletedAudioTime.Name = "DateTimePickerCompletedAudioTime";
            this.DateTimePickerCompletedAudioTime.ShowUpDown = true;
            this.DateTimePickerCompletedAudioTime.Size = new System.Drawing.Size(67, 20);
            this.DateTimePickerCompletedAudioTime.TabIndex = 1;
            this.DateTimePickerCompletedAudioTime.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateTimePickerCompletedAudioTime.ValueChanged += new System.EventHandler(this.DateTimePickerCompletedAudioTime_ValueChanged);
            // 
            // LabelCompletedAudioTime
            // 
            this.LabelCompletedAudioTime.AutoSize = true;
            this.LabelCompletedAudioTime.Location = new System.Drawing.Point(12, 18);
            this.LabelCompletedAudioTime.Name = "LabelCompletedAudioTime";
            this.LabelCompletedAudioTime.Size = new System.Drawing.Size(113, 13);
            this.LabelCompletedAudioTime.TabIndex = 2;
            this.LabelCompletedAudioTime.Text = "Completed Audio Time";
            // 
            // LabelPayPerMinute
            // 
            this.LabelPayPerMinute.AutoSize = true;
            this.LabelPayPerMinute.Location = new System.Drawing.Point(12, 40);
            this.LabelPayPerMinute.Name = "LabelPayPerMinute";
            this.LabelPayPerMinute.Size = new System.Drawing.Size(79, 13);
            this.LabelPayPerMinute.TabIndex = 3;
            this.LabelPayPerMinute.Text = "Pay Per Minute";
            // 
            // ButtonReset
            // 
            this.ButtonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonReset.Location = new System.Drawing.Point(197, 64);
            this.ButtonReset.Name = "ButtonReset";
            this.ButtonReset.Size = new System.Drawing.Size(75, 23);
            this.ButtonReset.TabIndex = 4;
            this.ButtonReset.Text = "Reset";
            this.ButtonReset.UseVisualStyleBackColor = true;
            this.ButtonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // LabelTotalPay
            // 
            this.LabelTotalPay.AutoSize = true;
            this.LabelTotalPay.Location = new System.Drawing.Point(12, 61);
            this.LabelTotalPay.Name = "LabelTotalPay";
            this.LabelTotalPay.Size = new System.Drawing.Size(97, 13);
            this.LabelTotalPay.TabIndex = 5;
            this.LabelTotalPay.Text = "Total Pay: $000.00";
            // 
            // LabelHourlyWage
            // 
            this.LabelHourlyWage.AutoSize = true;
            this.LabelHourlyWage.Location = new System.Drawing.Point(12, 77);
            this.LabelHourlyWage.Name = "LabelHourlyWage";
            this.LabelHourlyWage.Size = new System.Drawing.Size(108, 13);
            this.LabelHourlyWage.TabIndex = 6;
            this.LabelHourlyWage.Text = "Hourly Wage: $00.00";
            this.LabelHourlyWage.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 99);
            this.Controls.Add(this.LabelHourlyWage);
            this.Controls.Add(this.LabelTotalPay);
            this.Controls.Add(this.ButtonReset);
            this.Controls.Add(this.LabelPayPerMinute);
            this.Controls.Add(this.LabelCompletedAudioTime);
            this.Controls.Add(this.DateTimePickerCompletedAudioTime);
            this.Controls.Add(this.NumericUpDownPayPerMinute);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Speechpad Wage Tracker";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPayPerMinute)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown NumericUpDownPayPerMinute;
        private System.Windows.Forms.DateTimePicker DateTimePickerCompletedAudioTime;
        private System.Windows.Forms.Label LabelCompletedAudioTime;
        private System.Windows.Forms.Label LabelPayPerMinute;
        private System.Windows.Forms.Button ButtonReset;
        private System.Windows.Forms.Label LabelTotalPay;
        private System.Windows.Forms.Label LabelHourlyWage;
    }
}

